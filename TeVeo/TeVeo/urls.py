"""
URL configuration for TeVeo project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/5.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))

    path se usa para definir rutas URL,
    include se usa para incluir otros conjuntos de URLs.
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path("", include('teveo_app.urls')),
    path("admin/", admin.site.urls),
]
'''Incluye conjunto de rutas URL definidas en el archivo urls.py de la aplicación teveo_app. 
Se refiere al conjunto de URLs predefinidas por Django para el sitio de administración.'''