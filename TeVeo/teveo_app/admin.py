from django.contrib import admin
from .models import Comentarios, Camara

'''Registra los modelos con el sitio de administración de Django'''
admin.site.register(Camara)
admin.site.register(Comentarios)