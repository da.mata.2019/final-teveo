from django.db import models


class Camara(models.Model):
    id_camara = models.CharField(max_length=256)
    descripcion = models.CharField(max_length=256)
    url = models.URLField()
    latitud = models.FloatField()
    longitud = models.FloatField()
    num_comments = models.IntegerField(default=0)

    def __str__(self):
        return self.id_camara
# en el panel de administración de Django, se mostrará el valor del id_camara

class Comentarios(models.Model):
    titulo = models.CharField(max_length=256)
    id_camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    imagen = models.TextField()
    texto = models.TextField()
    fecha = models.DateTimeField()
    autor = models.TextField(max_length=100, default='Anónimo')

    def __str__(self):
        return self.titulo

