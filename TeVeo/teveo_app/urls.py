from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("comentario/", views.comentarios, name="comentario"),
    path('camaras/', views.camaras, name='camaras'),
    path('camaras/<str:camera_id>',views.camara_especifica, name="camara_especifica"),
    path('registro/', views.registro, name="registro"),
    path('help/', views.ayuda, name="ayuda")
]
