import datetime
from django.http import HttpResponse
import os
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from .models  import Camara, Comentarios
from xml.etree import cElementTree as ET
from django.conf import settings
from django.utils import timezone
import urllib.request
import base64

DB_SOURCE = [
    "listado1.xml",
    "listado2.xml"
]

def read_xml(file_name):
    cameras = [] # almacenará los datos de las cámaras leídas del archivo XML.
    tree = ET.parse(file_name) # lee el archivo XML especificado y crea un objeto ElementTree que contiene la jerarquía de elementos XML.
    root = tree.getroot()
    # El elemento raíz es el nodo superior del XML, el contenedor de todos los demás elementos.
    # Una vez que tienes el elemento raíz, puedes navegar por el resto del árbol XML.

    for cam in root.findall('cam'):  #Itera sobre todos los elementos <cam> en el XML 2
        cam_id = cam.get('id') # Obtiene el atributo id del elemento <cam>.
        url = cam.find('url').text #Obtiene el texto del subelemento <url>
        info = cam.find('info').text
        latitude = cam.find('place/latitude').text #Obtiene el texto del subelemento <latitude> dentro de <place>.
        longitude = cam.find('place/longitude').text

        #Crea un objeto Camara actualizando los campos con los valores obtenidos del XML.
        camera, created= Camara.objects.update_or_create(id_camara=cam_id, defaults={'descripcion': info, 'latitud': latitude, 'longitud': longitude,'url': url})

        #meto en la lista 'camaras' mi objeto camera
        cameras.append(camera)

    for camara in root.findall('camara'): #Itera sobre todos los elementos <camara> en el XML 1
        cam_id = camara.find('id').text #Obtiene el texto del subelemento <id>
        src = camara.find('src').text
        lugar = camara.find('lugar').text
        coordenadas = camara.find('coordenadas').text.split(',')
        latitude = coordenadas[0]
        longitude = coordenadas[1]

        camera ,created = Camara.objects.update_or_create(id_camara=cam_id, defaults={'descripcion': lugar, 'latitud': latitude, 'longitud': longitude,'url': src})
        cameras.append(camera)

    return cameras

#  cada función a la que llamar el urls preparan los datos que se deben pasar a sus respectivos templates
def index(request): #parámetro request es un objeto HttpRequest que contiene información sobre la solicitud que se ha realizado al servidor.
    comments = Comentarios.objects.all().order_by('-fecha')
    total_comments = Comentarios.objects.count()

    context = {
        'comments': comments,
        'total_comments' : total_comments
    }
    return render(request, 'main.html', context) # le pasa a main.html la solicitud que se ha hecho y los datos en el dicc context
    #la función index devuelve renderizado el objeto HttpResponse al servidor web.
    #El servidor web envía la respuesta al navegador del usuario
    # lo que envía es el mismo html con los datos

def download_image(url_imagen):
    """Download image and return it as bytes"""
    # me guardo en request la solicitud HTTP que se envia para obtener la imagen, a partir de la URL
    request = urllib.request.Request(url=url_imagen)

    # abro la URL y devuelvo la respuesta
    with urllib.request.urlopen(request) as response:
        # leo los datos de la respuesta y los almaceno
        image_bytes = response.read()

        if image_bytes:
            # codificamos la imagen en formato Base64
            image_base64 = base64.b64encode(image_bytes).decode('utf-8')

    return image_base64

def comentarios(request):
    if request.method == "POST":
        camera_id = request.POST.get('id_camara') #Obtiene el valor del campo id_camara del formulario enviado.
        comment_text = request.POST.get('texto')
        autor = request.session.get('autor', 'Anónimo')

        if not autor:
            autor = 'Anónimo'

        camera = Camara.objects.get(id_camara=camera_id) #Obtiene el objeto Camara correspondiente al id_camara proporcionado.
        img_url = camera.url #aqui tendría que descrgar esta imagen
        image_url_64 = download_image(img_url)
        if image_url_64:
            image_url = f"data:image/jpeg;base64,{image_url_64}"
        else:
            image_url = None
            print('No hay imagen disponible')


        comment = Comentarios(id_camara=camera, texto=comment_text, fecha=timezone.now(), imagen=image_url, autor=autor)
        comment.save() #guarda el nuevo comentario en la base de datos
        camera.num_comments += 1
        camera.save() #guarda en la base de datos
        return redirect('index') #una vez puesto el comentario me redirige a la página principal para ver mi comentario


    camara_id = request.GET['camera_id'] #Obtiene el camera_id de los parámetros de la solicitud GET.
    camera = Camara.objects.get(id_camara=camara_id) #Obtiene el objeto Camara correspondiente a esa id_camara
    comments = Comentarios.objects.filter(id_camara=camera) #Obtiene todos los comentarios asociados a la cámara.
    context = {
        'camera': camera,
        'comments': comments,
        "camera_count": Camara.objects.count(),
        "comment_count": Comentarios.objects.count()
    }
    return render(request, "comentarios.html", context)


@csrf_exempt  #desactivar la protección CSRF (Cross-Site Request Forgery)
def camaras(request):
    camaras = Camara.objects.all()
    for camara in camaras:
        camara = Camara.objects.get(id_camara=camara.id_camara)
        comentarios = Comentarios.objects.filter(id_camara=camara) #Obtiene todos los comentarios asociados con la cámara actual.
        camara.num_comments = comentarios.count()
        camara.save()

    camaras = Camara.objects.all().order_by('-num_comments') #ordenio las camaras de mas a menos comentarios
    context = {
        "source": DB_SOURCE,
        "camaras": camaras,
        }
    return render(request, 'camaras.html', context)

def camara_especifica(request, camera_id):
    camara = Camara.objects.get(id_camara=camera_id)
    comentarios = Comentarios.objects.all()
    context = {
        "camara": camara,
        "comentarios": comentarios,
    }
    return render(request, 'camara_especifica.html', context)


def registro(request):
    return render(request, 'registro.html')

def ayuda(request):
    context = {
        'pagina': "Ayuda",
        'total_comments': Comentarios.objects.count(),
    }
    return render(request, "ayuda.html", context)