# Final-TeVeO

Repositorio de plantilla para el proyecto final del curso 2023-2024.

Para entregar el proyecto depositado en este repositorio, leer con atención el apartado sobre la entrega del enunciado del ejercicio. Antes de entregar, rellenar el texto que viene a continuación según indica dicho enunciado, y borrar toda esta primera parte.

<<<<<BORRAR HASTA AQUí, INCLIUDA ESTA LÍNEA>>>>>

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Denisse Andrea Mata Topping
* Titulación: Doble Grado en Ingeniería en Sistemas de Telecomunicación y ADE
* Cuenta en laboratorios: denissem
* Cuenta URJC: da.mata.2019
* Video básico (url): https://youtu.be/vxxWxBhPwCk?si=uoHi7iYcStISVWFl
* Video parte opcional (url): https://www.youtube.com/watch?v=IndYrMl3GB0
* Despliegue (url):  denissem.pythonanywhere.com 
* Contraseñas:
* Cuenta Admin Site: usuario/contraseña

## Resumen parte obligatoria

## Lista partes opcionales

* Nombre parte:
* Nombre parte:
* ...
